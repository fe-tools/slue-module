# slue-module

A tool of analyse modules relationship.

## API
```javascript
const slueModule = require('slue-module');
let realyData = slueModule(options);
```

## options：
```javascript
{
    // the file to analyse
    filePath: './test/app.js',

    root: '/',

    prevHandler() {}

    // external global variables
    externals: {
        'react': 'React'
    },

    // when exclude return true, the required module will be ignore
    exclude: function(opts) {
        return opts.moduleId == 'xxx';
    },

    // the ext names of the files which to analyse
    ext: ['.js', '.jsx', '.css', '.less', '.html'],

    // alias make module require shorter
    alias: {
        components: path.join(__dirname, './components')
    }
};
```

## output:
```javascript
{
    // file path and it's require id
    moduleList: [
        {
            id: 'xxx', // require id
            dir: 'xxx' // file path
        }
    ],

    // all handled files, used file path to be key
    handledFile: {}
};
```