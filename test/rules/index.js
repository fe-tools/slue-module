const fs = require('fs');
const rule = /(?:[^\w])(?:require\(('|")([@\w./-]+)\1\)|(?:import|export)\s*(?:[\w\s,\-*{}]+)?from\s*('|")([@\w./-]+)\3|import\(?\s*('|")([@\w./-]+)\5\)?)/g;
const content = ` ${fs.readFileSync('./app.js', 'utf8')}`;
console.log(content.match(rule));