const fs = require('fs');
const path = require('path');
const util = require('util');
const chalk = require('chalk');
const utils = require('./libs/utils');
const getConfig = require('./config.js');
const writeLog = process.argv.slice(2).indexOf('-l') != -1;

function searchModule(filePath, config, globalData) {
  globalData = globalData || {
    handledFile: {}
  };

  globalData.handledFile[filePath] = {};

  let data = fs.readFileSync(filePath);

  data = data.toString();
  data = data.replace(/\/\*[\w\W]*?\*\//g, '');
  data = data.replace(/\/\/.*/g, '');
  data = ` ${data}`;

  if (typeof config.prevHandler == 'function') {
    data = config.prevHandler({
      path: filePath,
      content: data
    });
  }

  let extname = path.extname(filePath);

  let moduleRegular = /(?:[^\w])(?:require\(('|")([@\w./-]+)\1\)|(?:import|export)\s*(?:[\w\s,\-*{}]+)?from\s*('|")([@\w./-]+)\3)/g;
  let itemRegular = /(?:[^\w])(?:require\(('|")([@\w./-]+)\1\)|(?:import|export)\s*(?:[\w\s,\-*{}]+)?from\s*('|")([@\w./-]+)\3)/;
  matchResult = data.match(moduleRegular);
  if(matchResult) {
    matchResult.forEach(function(item) {
      let itemMatchRes = item.match(itemRegular);
      if(itemMatchRes) {
        let moduleId = itemMatchRes[2] || itemMatchRes[4] || itemMatchRes[6];
        let isExclude = config.exclude ? config.exclude({
          moduleId: moduleId
        }) : false;
        if(isExclude == false) {
          // 查找当前文件下是否处理过当前moduleId，避免重复扫描文件
          let theModule = globalData.handledFile[filePath][moduleId];
          if(!theModule) {
            // 全局变量配置
            if(config.externals[moduleId]) {
              globalData.handledFile[filePath][moduleId] = config.externals[moduleId];
              globalData.handledFile[filePath].externals = globalData.handledFile[filePath].externals || {};
              globalData.handledFile[filePath].externals[moduleId] = config.externals[moduleId];
              return;
            }

            let moduleDir = moduleId;
            let dirname = path.dirname(filePath);

            // config.alias
            let theConfigAlia = utils.findConfigAlias(moduleId, config);
            if(theConfigAlia) {
              moduleDir = moduleDir.replace(theConfigAlia.shortPath, '.');
              moduleDir = path.resolve(theConfigAlia.fullPath, moduleDir);
            } else if(utils.isRelative(moduleId)) {
              // 相对路径
              moduleDir = path.resolve(dirname, moduleId);
            } else if(utils.isNodeModule(moduleId)) {
              // node_modules
              moduleDir = utils.getNodeModulePath(dirname, moduleId, config);
            }

            // 尝试补充默认文件名index及文件后缀名
            if(moduleDir) {
              let moduleDirParser = path.parse(moduleDir);
              if(config.ext.indexOf(moduleDirParser.ext) == -1) {
                moduleDir = utils.tryExtension(moduleDir, config);
              }
            }

            if(moduleDir && fs.existsSync(moduleDir)) {
              globalData.handledFile[filePath][moduleId] = moduleDir;

              if(!globalData.handledFile[moduleDir]) {
                searchModule(moduleDir, config, globalData);
              }
            } else {
              util.log(chalk.red(`module ${moduleId} used in ${filePath} is not exist`));
              process.exit(1);
            }
          }
        }
      }
    });
  }

  return globalData;
}

module.exports = function(config) {
  // handle config
  if(!config.filePath) {
    util.log(chalk.red('need filePath'));
    process.exit(1);
  }

  config = getConfig(config);
  config.filePath = path.resolve(process.cwd(), config.filePath);

  if(!fs.existsSync(config.filePath)) {
    util.log(chalk.red(`the entry file ${config.filePath} is not exist!`));
    process.exit(1);
  }

  let t1 = new Date().getTime();
  if(writeLog) {
    console.log(`********** slue-module start **********`);
  }
  let relayData = searchModule(config.filePath, config);
  let t2 = new Date().getTime();

  if(writeLog) {
    console.log(`${(t2 - t1) / 1000}s`, config.filePath);
    console.log(`********** slue-module end **********\n\r`);
  }

  if(relayData) {
    return relayData;
  }
}