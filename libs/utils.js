const path = require('path');
const fs = require('fs');

module.exports = {
    isRelative(path) {
        let reg = /^\./;
        return reg.test(path);
    },
    isNodeModule(path) {
        let reg = /^[^\/\.]/;
        return reg.test(path);
    },
    getNodeModulePath(dirname, moduleId, config) {
        let moduleIdItems = moduleId.split('/');

        // 查找模块的package.json文件
        let modName = moduleIdItems[0];
        let packagePath = path.resolve(dirname, './node_modules', modName, './package.json');
        let isPackageJsonExists = false;
        while (dirname != config.root) {
            if (fs.existsSync(packagePath)) {
                isPackageJsonExists = true;
                break
            }
            dirname = path.resolve(dirname, '../');
            packagePath = path.resolve(dirname, './node_modules', modName, './package.json');
        }

        if (isPackageJsonExists) {
            // 引用某个node安装包： require('zrender')
            if (moduleIdItems.length === 1) {
                const packageJSON = require(packagePath);
                // cdn公共库
                if (packageJSON.jsdelivr) {
                    return path.resolve(dirname, './node_modules', modName, packageJSON.jsdelivr);
                } else if (packageJSON.main) {
                    const main = packageJSON.main.replace(/^\//, '');
                    return path.resolve(dirname, './node_modules', modName, main);
                } else if (packageJSON.module) {
                    if (typeof packageJSON.browser == 'string') {
                        return path.resolve(dirname, './node_modules', modName, packageJSON.browser);
                    } else if (typeof packageJSON.browser == 'object') {
                        if (packageJSON.browser[packageJSON.module]) {
                            return path.resolve(dirname, './node_modules', modName, packageJSON.browser[packageJSON.module]);
                        } else if (packageJSON.browser[packageJSON.main]) {
                            return path.resolve(dirname, './node_modules', modName, packageJSON.browser[packageJSON.main]);
                        }
                    }
                    return path.resolve(dirname, './node_modules', modName, packageJSON.module);
                }
                return path.resolve(dirname, './node_modules', modName, './index');
            } else {
                // 引用某个node安装包下的某个文件： require('zrender/libs/utils')
                return path.resolve(dirname, './node_modules', './', moduleId);
            }
        }
    },
    tryExtension(moduleDir, config) {
        // 单纯尝试补充后缀
        let exts = config.ext.slice(0);
        while (exts.length > 0) {
            let ext = exts.shift();
            let _moduleDir = `${moduleDir}${ext}`;
            if (fs.existsSync(_moduleDir)) {
                return _moduleDir;
            }
        }

        // 补充默认文件名index+后缀名
        exts = config.ext.slice(0);
        while (exts.length > 0) {
            let ext = exts.shift();
            let _moduleDir = path.resolve(moduleDir, `./index${ext}`);
            if (fs.existsSync(_moduleDir)) {
                return _moduleDir;
            }
        }

        return false;
    },
    findConfigAlias(moduleId, config) {
        if (config.alias) {
            for (let k in config.alias) {
                if (moduleId.indexOf(k) === 0) {
                    return {
                        shortPath: k,
                        fullPath: config.alias[k]
                    };
                }
            }
        }
        return false;
    }
}
